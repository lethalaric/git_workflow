GIT AND GITLAB

1. Git workflow
Every git repositories, basic git workflow is needed to make sure the repositories work. This basic workflow are add working files or folders to staging area, commit it to local repos and if its already confident with the changes, push to remote repos for another member to take actions. Please take a look to below illustration.
![](images/git_workflow.jpg)

2. Git .gitignore
Sometimes, we want to exclude files or folders from git so that git would not check the changes on those files or folders. To do so, we use .gitignore to add excluded files or folders.
	1. Create file .gitignore
        ```
        $ touch .gitignore
        $ ll
        total 12
        drwxr-xr-x 3 root root 4096 Mar 27 10:52 ./
        drwxr-xr-x 3 root root 4096 Mar 27 10:52 ../
        drwxr-xr-x 7 root root 4096 Mar 27 10:52 .git/
        -rw-r--r-- 1 root root    0 Mar 27 10:52 .gitignore
        $ cat .gitignore 
        $ 
        ```
    2. Add files or folders for excluded
        ```
        $ cat .gitignore 
        images/*
        dummy_file.txt
        ```
    3. Check it with git status  
        before :  
        ```
        $ git status
        On branch master

        No commits yet

        Untracked files:
        (use "git add <file>..." to include in what will be committed)

            .gitignore
            README.md
            dummy_file.txt
            images/

        nothing added to commit but untracked files present (use "git add" to track)
        $ 

        ```
        after
        ```
        $ git status
        On branch master

        No commits yet

        Untracked files:
        (use "git add <file>..." to include in what will be committed)

            .gitignore
            README.md

        nothing added to commit but untracked files present (use "git add" to track)
        $ 

        ```

3. Git checkout  
Most used git checkout when collaborating is checking out to remote branch or checking out to certain commits.  
    1. Checkout to remote branch
        ```
        $ git checkout -b YOUR_LOCAL_BRANCH origin/YOUR_REMOTE_BRANCH
        ```
    2. Checkout to commit
        ```
        $ git log
        commit 1d1d4d6808ac084b5abafe53595f2d0eae8d4d48 (HEAD -> development)
        Author: Mukhtar Haris <email@gmail.com>
        Date:   Fri Mar 27 11:52:41 2020 +0700

            update README.md

        commit 1f1c3882c0d3aeb63b72954fced36ac74391c74d
        Author: Mukhtar Haris <email@gmail.com>
        Date:   Fri Mar 27 11:52:12 2020 +0700

            exclude file and folder

        $ git checkout 1f1c3882c0d3aeb63b72954fced36ac74391c74d
        Note: checking out '1f1c3882c0d3aeb63b72954fced36ac74391c74d'.

        You are in 'detached HEAD' state. You can look around, make experimental
        changes and commit them, and you can discard any commits you make in this
        state without impacting any branches by performing another checkout.

        If you want to create a new branch to retain commits you create, you may
        do so (now or later) by using -b with the checkout command again. Example:

        git checkout -b <new-branch-name>

        HEAD is now at 1f1c388 exclude file and folder

        $ git log
        commit 1f1c3882c0d3aeb63b72954fced36ac74391c74d (HEAD)
        Author: Mukhtar Haris <email@gmail.com>
        Date:   Fri Mar 27 11:52:12 2020 +0700

            exclude file and folder
        $ 
        ```

4. Gitlab workflow  
This is workflow in gitlab that I suggest to do for JOSS.  
    4.1. Create issue  
    Create issue related to what you want to working on.  
    ![](images/gitlab_create_issue.png)  
    4.2. Create branch from parent  
    Create a branch from issue.  
    ![](images/gitlab_create_branch.png)  
    4.3. Merge request  
    After you are confident with your work, you can merge request to be merge with parent branch.  
    ![](images/gitlab_merge_request.png)  
    4.4. CICD pipeline  
    Last sequence of gitlab workflow I suggest is using gitlab cicd to do thing like packaging, testing or even send project to repositories. For joss, I suggest to packaging the production branch to .jar so if we need the file, we can easily download it without another packaging or testing because the gitlab cicd has cover it up.  
    4.4.1. Create file .gitlab-ci.yml  
    4.4.2. Insert this to .gitlab-ci.yml    
    ```
    image: maven:latest
    build:
        stage: build
        script:
            - cd demo/
            - mvn clean package -DskipTests
        only:
            - development
        artifacts:
            paths:
                - demo/target/*.jar
    ```
    4.4.3. Artifact output
    - Output of gitlab-ci pipeline.  
    ![](images/gitlab_pipeline_result.png)
    - What contains on pipeline's artifact
    ![](images/gitlab_artifact.png)

5. Why proposed workflow is important?  
- Create issue will track what happen as summaries to repository instead of reading all commit messages and figure out what happen.   
- Merge request to parent branch will group and track changes that still on the same working issue.  
- Merge request also make it easy if the working branch is a mess and want to revert.  
- Gitlab cicd, even just its small portion that used, it make sure the joss is successfully packaged and keep track its artifacts so it will be easy to get the latest production branch's jar

6. What are another ideas that can be implemented?
- Make use of gitlab tag and gitlab release
- Make use of gilab cicd

7. References 
- https://docs.gitlab.com/ee/README.html